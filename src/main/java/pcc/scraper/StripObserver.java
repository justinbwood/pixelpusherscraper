package pcc.scraper;

import java.util.Observable;
import java.util.Observer;

import com.heroicrobot.dropbit.registry.DeviceRegistry;

public class StripObserver implements Observer {
	public boolean hasStrips = false;
	public void update(Observable registry, Object updatedDevice) {
		this.hasStrips = true;
		DeviceRegistry r = (DeviceRegistry) registry;
		r.startPushing();
		r.setAutoThrottle(true);
		r.setLogging(false);
		System.out.println("Started PixelPusher.");
	}
}
