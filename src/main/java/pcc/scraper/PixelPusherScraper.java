package pcc.scraper;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;

import pcc.scraper.spi.ScraperHandler;
import pcc.util.Pair;

import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import com.heroicrobot.dropbit.registry.DeviceRegistry;

public class PixelPusherScraper implements ScraperHandler {

	private static final String serviceName = "com.nullghost.pcc.PixelPusherScraper";
	private static final String[] colorModes = { "RGB", "RGBOW", "HSB" };
	private String currentColorMode = "RGB";

	static DeviceRegistry registry;
	static StripObserver stripObserver;
	List<Strip> strips;

	public PixelPusherScraper() {
		registry = new DeviceRegistry();
		stripObserver = new StripObserver();
		registry.addObserver(stripObserver);
	}
	
	public void load() {
		strips = registry.getStrips();
	}
	
	public void unload() {
		
	}
	
	public String getServiceName() {
		return serviceName;
	}

	public HashMap<String, String> getConfiguration() {
		// TODO Implement configuration service, then this
		return null;
	}

	public void setConfiguration(HashMap<String, String> configMap) {
		// TODO Implement configuration service, then this
	}

	public Pair<Integer, Integer> getDimensions() {
		// TODO Remove hard coding, use configuration
		return new Pair<Integer, Integer>(1, 240);
	}

	public boolean setColorMode(String mode) {
		for (String s : colorModes) {
			if (s.equals(mode)) {
				currentColorMode = new String(mode);
				return true;
			}
		}
		return false;
	}

	public int getFPSLimit(int h, int w) {
		// TODO Fix this to detect based on display size
		return 120;
	}

	public void pushPixel(int x, int y, Color pix) {
		if (currentColorMode.equals("RGB")) {
			strips.get(y).setPixel(pix.getRGB(), x);
		}
	}

	public void pushHorizontalLine(int y, Color[] pix) {
		Strip s = strips.get(y);
		if (currentColorMode.equals("RGB")) {
			for (int i = 0; i < pix.length; i++) {
				s.setPixel(pix[i].getRGB(), i);
			}
		}
	}

	public void pushVerticalLine(int x, Color[] pix) {
		if (currentColorMode.equals("RGB")) {
			for (int i = 0; i < pix.length; i++) {
				strips.get(i).setPixel(pix[i].getRGB(), x);
			}
		}
	}

	public void pushFrame(Color[][] pixels) {
		for (int i = 0; i < pixels.length; i++) {
			pushHorizontalLine(i, pixels[i]);
		}
	}

}
